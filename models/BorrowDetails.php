<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "borrow_details".
 *
 * @property int $id
 * @property int $id_inventaris
 * @property int $jumlah
 *
 * @property Inventories $inventaris
 */
class BorrowDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'borrow_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_inventaris', 'jumlah'], 'required'],
            [['id_inventaris', 'jumlah'], 'integer'],
            [['id_inventaris'], 'exist', 'skipOnError' => true, 'targetClass' => Inventories::className(), 'targetAttribute' => ['id_inventaris' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_inventaris' => Yii::t('app', 'Barang'),
            'jumlah' => Yii::t('app', 'Jumlah'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventaris()
    {
        return $this->hasOne(Inventories::className(), ['id' => 'id_inventaris']);
    }
}
