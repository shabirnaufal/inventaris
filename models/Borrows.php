<?php

namespace app\models;

use Yii;
use app\models\BorrowDetails;

/**
 * This is the model class for table "borrows".
 *
 * @property int $id
 * @property string $tanggal_pinjam
 * @property string $tanggal_kembali
 * @property string $status_peminjaman
 * @property int $id_pegawai
 *
 * @property Employees $pegawai
 */
class Borrows extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'borrows';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tanggal_pinjam', 'tanggal_kembali'], 'safe'],
            [['status_peminjaman', 'id_pegawai'], 'required'],
            [['id_pegawai'], 'integer'],
            [['status_peminjaman','kode_peminjaman'], 'string', 'max' => 255],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['id_pegawai' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'id' => Yii::t('app', 'ID'),
            'kode_peminjaman' => Yii::t('app', 'Kode Peminjaman'),
            'tanggal_pinjam' => Yii::t('app', 'Tanggal Pinjam'),
            'tanggal_kembali' => Yii::t('app', 'Tanggal Kembali'),
            'status_peminjaman' => Yii::t('app', 'Status Peminjaman'),
            'id_pegawai' => Yii::t('app', 'Id Pegawai'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Employees::className(), ['id' => 'id_pegawai']);
    }
    public function getPolisi()
    {
        return $this->hasMany(BorrowDetails::className(), ['id' => 'id']);
    }

}
