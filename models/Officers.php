<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "officers".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property int $level
 *
 * @property Inventories[] $inventories
 * @property Levels $level0
 */
class Officers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'officers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'level'], 'required'],
            [['level'], 'integer'],
            [['username', 'password', 'name'], 'string', 'max' => 225],
            [['level'], 'exist', 'skipOnError' => true, 'targetClass' => Levels::className(), 'targetAttribute' => ['level' => 'id_level']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'name' => Yii::t('app', 'Name'),
            'level' => Yii::t('app', 'Level'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventories::className(), ['id_petugas' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel0()
    {
        return $this->hasOne(Levels::className(), ['id_level' => 'level']);
    }
}
