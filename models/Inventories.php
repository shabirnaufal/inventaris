<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventories".
 *
 * @property int $id
 * @property string $nama
 * @property string $kondisi
 * @property string $keterangan
 * @property int $jumlah
 * @property int $id_jenis
 * @property string $tanggal_register
 * @property int $id_ruang
 * @property int $kode_inventaris
 * @property int $id_petugas
 *
 * @property BorrowDetail[] $borrowDetails
 * @property Officers $petugas
 * @property Rooms $ruang
 * @property Types $jenis
 */
class Inventories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'kondisi', 'keterangan', 'jumlah', 'id_jenis', 'id_ruang', 'kode_inventaris', 'id_petugas'], 'required'],
            [['jumlah', 'id_jenis', 'id_ruang', 'id_petugas'], 'integer'],
            [['tanggal_register'], 'safe'],
            [['nama', 'kondisi'], 'string', 'max' => 225],
            [['keterangan'], 'string', 'max' => 255],
            [['id_petugas'], 'exist', 'skipOnError' => true, 'targetClass' => Officers::className(), 'targetAttribute' => ['id_petugas' => 'id']],
            [['id_ruang'], 'exist', 'skipOnError' => true, 'targetClass' => Rooms::className(), 'targetAttribute' => ['id_ruang' => 'id']],
            [['id_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => Types::className(), 'targetAttribute' => ['id_jenis' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'kondisi' => Yii::t('app', 'Kondisi'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'jumlah' => Yii::t('app', 'Jumlah'),
            'id_jenis' => Yii::t('app', 'Jenis'),
            'tanggal_register' => Yii::t('app', 'Tanggal Register'),
            'id_ruang' => Yii::t('app', 'Ruangan'),
            'kode_inventaris' => Yii::t('app', 'Kode Inventaris'),
            'id_petugas' => Yii::t('app', 'Petugas'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBorrowDetails()
    {
        return $this->hasMany(BorrowDetail::className(), ['id_inventaris' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetugas()
    {
        return $this->hasOne(Officers::className(), ['id' => 'id_petugas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuang()
    {
        return $this->hasOne(Rooms::className(), ['id' => 'id_ruang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasOne(Types::className(), ['id' => 'id_jenis']);
    }
}
