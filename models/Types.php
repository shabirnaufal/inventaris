<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "types".
 *
 * @property int $id
 * @property string $name
 * @property int $kode_jenis
 * @property string $keterangan
 *
 * @property Inventories[] $inventories
 */
class Types extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'kode_jenis', 'keterangan'], 'required'],
            [['kode_jenis'], 'integer'],
            [['name', 'keterangan'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nama Jenis'),
            'kode_jenis' => Yii::t('app', 'Kode Jenis'),
            'keterangan' => Yii::t('app', 'Keterangan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventories::className(), ['id_jenis' => 'id']);
    }
}
