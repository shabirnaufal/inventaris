<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property string $nama_ruang
 * @property int $kode_ruang
 * @property string $keterangan
 *
 * @property Inventories[] $inventories
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_ruang', 'kode_ruang', 'keterangan'], 'required'],
            [['kode_ruang'], 'integer'],
            [['nama_ruang', 'keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_ruang' => Yii::t('app', 'Nama Ruang'),
            'kode_ruang' => Yii::t('app', 'Kode Ruang'),
            'keterangan' => Yii::t('app', 'Keterangan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(Inventories::className(), ['id_ruang' => 'id']);
    }
}
