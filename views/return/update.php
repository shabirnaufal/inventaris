<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Borrows */

$this->title = Yii::t('app', 'Kembalikan Pinjaman: {name}', [
    'name' => $model->pegawai->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Borrows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="borrows-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <br><br>
    <?= $this->render('_form', [
        'model' => $model,
        'details' => $details
    ]) ?>

</div>
