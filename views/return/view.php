<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Borrows */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Borrows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="borrows-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'kode_peminjaman',
        [
            'attribute' => 'pegawai.name',
            'header' => 'Peminjam',
        ],
        [
            'attribute' => 'tanggal_pinjam',
            'format' => [ 'date', 'php: d-M-Y' ],
        ],
        [
            'attribute' => 'tanggal_kembali',
            'format' => [ 'date', 'php: d-M-Y' ],
        ],
        'status_peminjaman',
    ],
]) ?>
 
<div class="item panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title pull-left"><i class="glyphicon glyphicon-barcode"></i> Transaction Line Item</h3>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $details,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
 
                [
                    'attribute' => 'id_inventaris',
                    'value' => 'inventaris.nama',
                    'header' => 'Nama Barang',
                ],
                'jumlah',
            ],
        ]); ?>
    </div>
</div>

</div>
