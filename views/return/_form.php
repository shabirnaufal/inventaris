<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Employees;
use app\models\Inventories;

/* @var $this yii\web\View */
/* @var $model app\models\Borrows */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="borrows-form">

    <?php $form = ActiveForm::begin(['id' => 'borrows-form']); ?>
      <div class="row">        
            <div class="col-sm-4 col-md-3">
                    <?= $form->field($model, 'kode_peminjaman')->textInput(['maxlength' => true,'disabled' => true]) ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php 
                    echo '<label class="control-label" for="tanggal_kembali">Tanggal Kembali</label>';
                    echo DatePicker::widget([
                        'id' => 'tanggal_kembali',
                        'name' => 'Borrows[tanggal_kembali]',
                        'value' => date('Y-m-d'),
                        'disabled' => true
                    ]);
                ?>
            </div>


            <div class="col-sm-4 col-md-3">
                <?= $form->field($model, 'status_peminjaman')->dropDownList(
                    ['kembali' => 'Kembali'],
                    ['prompt'=>'* Pilih Status *']                          
                ); ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?= $form->field($model, 'id_pegawai')->dropDownList(
                    ArrayHelper::map(Employees::find()->all(), 'id', 'name'),  
                    ['prompt'=>'* Pilih Pegawai *','disabled' => true]                          
                ); ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-th-list"></i> Detail Peminjaman</h4></div>
            <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', 
                        'widgetBody' => '.container-items',         
                        'widgetItem' => '.item',                     // required: css class
                        'limit' => 999,                                // the maximum times, an element can be cloned (default 999)
                        'min' => 1,                                  // 0 or 1 (default 1)
                        'insertButton' => '.add-item',               // css class
                        'deleteButton' => '.remove-item',            // css class
                        'model' => $details[0],
                        'formId' => 'borrows-form',
                        'formFields' => [
                            'id',
                            'id_inventaris',
                            'jumlah',
                        ],
                    ]);?>
                    <div class="container-items"><!-- widgetContainer -->
                        <?php foreach ($details as $i => $detail): ?>
                            <div class="item row">
                                <?php
                                    // necessary for update action.
                                    if (! $detail->isNewRecord) {
                                        echo Html::activeHiddenInput($detail, "[{$i}]id");
                                    }
                                ?>
                                <!-- <div class="col-sm-4 col-md-2">
                                    <?= $form->field($detail, "[{$i}]borrow_id")->textInput(['maxlength' => true]) ?>
                                </div> -->
                                <div class="col-sm-8 col-md-4">
                                    <?= $form->field($detail, "[{$i}]id_inventaris")->dropDownList(
                                        ArrayHelper::map(Inventories::find()->all(), 'id', 'nama'),  // Flat array ('id'=>'label')
                                        ['prompt'=>'* Pilih Barang *','disabled' => true,'label' => 'Barang']                          // options
                                    ); ?>
                                </div>
                         
                                <div class="col-sm-4 col-md-2">
                                    <?= $form->field($detail, "[{$i}]jumlah")->textInput(['maxlength' => true,'disabled' => true]) ?>
                                </div>
                                <div class="col-sm-2 col-md-1 item-action">
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs">
                                            <i class="glyphicon glyphicon-plus"></i></button> 
                                        <button type="button" class="remove-item btn btn-danger btn-xs">
                                            <i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                </div>
                            </div><!-- .row -->
            
                        <?php endforeach; ?>
                        </div>
            
                        <?php DynamicFormWidget::end(); ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
