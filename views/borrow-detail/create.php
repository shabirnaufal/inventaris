<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\borrowDetails */

$this->title = Yii::t('app', 'Create Borrow Details');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Borrow Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="borrow-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
