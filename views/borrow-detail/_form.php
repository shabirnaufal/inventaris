<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\borrowDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="borrow-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_inventaris')->textInput() ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
