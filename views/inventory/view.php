<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inventories */

$this->title = $model->nama; 

\yii\web\YiiAsset::register($this);
?>
<div class="inventories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Back to List'), ['index'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kode_inventaris',
            'nama',
            'kondisi',
            'keterangan',
            'jumlah',
            'jenis.name',
            [
                'attribute' => 'tanggal_register',
                'format' => [ 'date', 'php: d-m-Y' ],
                'labelColOptions' => [ 'style'=>'width:30%; text-align:right;' ]
            ],
            'ruang.name',
            'petugas.name',
        ],
    ]) ?>

</div>
