<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\date\DatePicker;
use app\models\Types;
use app\models\Officers;
use app\models\Rooms;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Inventories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_inventaris')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kondisi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'id_jenis')->dropDownList(
		    ArrayHelper::map(Types::find()->all(), 'id', 'name'),  // Flat array ('id'=>'label')
		    ['prompt'=>'* Pilih Jenis Barang *']                          // options
        );  
    ?>
    <?php 
        echo '<label class="control-label" for="tanggal_register">Tanggal</label>';
		echo DatePicker::widget([
			'id' => 'tanggal_register',
			'name' => 'Inventories[tanggal_register]',
            'value' => date('Y-m-d'),
            'disabled' => true
        ]);
    ?>
    <?= $form->field($model, 'id_ruang')->dropDownList(
		    ArrayHelper::map(Rooms::find()->all(), 'id', 'name'),  // Flat array ('id'=>'label')
		    ['prompt'=>'* Pilih Ruangan *']                          // options
        );
    ?>
    <?= $form->field($model, 'id_petugas')->dropDownList(
		    ArrayHelper::map(Officers::find()->all(), 'id', 'name'),  // Flat array ('id'=>'label')
		    ['prompt'=>'* Pilih Petugas *']                          // options
        );
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
