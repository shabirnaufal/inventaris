<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Types;
use app\models\Officers;
use app\models\Rooms;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\InventoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Inventories');

?>
<div class="inventories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Inventories'), ['create'], ['class' => 'btn btn-success']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php 
        if (Yii::$app->user->getId() == 1) {
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'filename' => 'Laporan Barang',
                'options' => [
                    'title' => 'DATA SARANA DAN PRASARANA SMK POLISI',
                    'subject' => 'DATA SARANA DAN PRASARANA SMK POLISI',
                    'keywords' => 'krajee, grid, export, yii2-grid, pdf'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'kode_inventaris',
                    'nama',
                    'kondisi',
                    'jumlah',
                    // 'keterangan',
                    [   
                        'attribute' => 'jenis.name', 
                        'filter' => Html::activeDropDownList($searchModel, 'id_jenis', ArrayHelper::map(Types::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']), 
                        'label' => 'Jenis Barang', 
                        'value' => function ($model, $index, $widget) { return $model->jenis->name; }
                    ],
                    // 'tanggal_register',
                    [   
                        'attribute' => 'ruang.name', 
                        'filter' => Html::activeDropDownList($searchModel, 'id_ruang', ArrayHelper::map(Rooms::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']), 
                        'label' => 'Ruangan', 
                        'value' => function ($model, $index, $widget) { return $model->ruang->name; }
                    ],
                    [   
                        'attribute' => 'petugas.name', 
                        'filter' => Html::activeDropDownList($searchModel, 'id_petugas', ArrayHelper::map(Officers::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']),  
                        'label' => 'Nama Petugas', 
                        'value' => function ($model, $index, $widget) { return $model->petugas->name; }
                    ],

                    ['class' => 'yii\grid\ActionColumn'],]
            ]);
        }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'],

            // 'kode_inventaris',
            'nama',
            'kondisi',
            'jumlah',
            // 'keterangan',
            [   
                'attribute' => 'jenis.name', 
                'filter' => Html::activeDropDownList($searchModel, 'id_jenis', ArrayHelper::map(Types::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']), 
                'label' => 'Jenis Barang', 
                'value' => function ($model, $index, $widget) { return $model->jenis->name; }
            ],
            // 'tanggal_register',
            [   
                'attribute' => 'ruang.name', 
                'filter' => Html::activeDropDownList($searchModel, 'id_ruang', ArrayHelper::map(Rooms::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']), 
                'label' => 'Ruangan', 
                'value' => function ($model, $index, $widget) { return $model->ruang->name; }
            ],
            [   
                'attribute' => 'petugas.name', 
                'filter' => Html::activeDropDownList($searchModel, 'id_petugas', ArrayHelper::map(Officers::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']),  
                'label' => 'Nama Petugas', 
                'value' => function ($model, $index, $widget) { return $model->petugas->name; }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
