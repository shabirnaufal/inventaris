<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inventories */

$this->title = Yii::t('app', 'Update Inventories: {name}', [
    'name' => $model->nama,
]);

?>
<div class="inventories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
