<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Borrows */

$this->title = Yii::t('app', 'Create Borrows');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Borrows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="borrows-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'details' => $details
    ]) ?>

</div>
