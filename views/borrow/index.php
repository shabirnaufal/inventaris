<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Employees;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\BorrowsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Peminjaman');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="borrows-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Peminjaman Baru'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php 
        if (Yii::$app->user->getId() == 1) {
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'filename' => 'Laporan Peminjaman',
                'options' => [
                    'title' => 'DATA SARANA DAN PRASARANA SMK POLISI',
                    'subject' => 'DATA SARANA DAN PRASARANA SMK POLISI',
                    'keywords' => 'krajee, grid, export, yii2-grid, pdf'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'kode_peminjaman',
                        'label' => 'Kode Peminjaman'
                    ],
                    [   
                        'attribute' => 'pegawai.name', 
                        'filter' => Html::activeDropDownList($searchModel, 'id', ArrayHelper::map(Employees::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']),  
                        'label' => 'Nama Pegawai', 
                        'value' => function ($model, $index, $widget) { return $model->pegawai->name; }
                    ],
                    [
                        'attribute' => 'tanggal_pinjam',
                        'label' => 'Tanggal Pinjam',
                        'format' => [ 'date', 'php: d-m-Y' ],
                    ],
                    // [
                    //     'attribute' => 'tanggal_kembali',
                    //     'format' => [ 'date', 'php: d-m-Y' ],
                    // ],
                    [
                        'attribute' => 'status_peminjaman',
                        'filter' => false 
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}   {update}'
                    ],
                ],
            ]);
        }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'kode_peminjaman',
                'label' => 'Kode Peminjaman'
            ],
            [   
                'attribute' => 'pegawai.name', 
                'filter' => Html::activeDropDownList($searchModel, 'id', ArrayHelper::map(Employees::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '']),  
                'label' => 'Nama Pegawai', 
                'value' => function ($model, $index, $widget) { return $model->pegawai->name; }
            ],
            [
                'attribute' => 'tanggal_pinjam',
                'label' => 'Tanggal Pinjam',
                'format' => [ 'date', 'php: d-m-Y' ],
            ],
            // [
            //     'attribute' => 'tanggal_kembali',
            //     'format' => [ 'date', 'php: d-m-Y' ],
            // ],
            [
                'attribute' => 'status_peminjaman',
                'filter' => false 
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}   {update}'
            ],
        ],
    ]); ?>


</div>
