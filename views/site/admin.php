<?php


/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Selamat Datang</h1>

        <p class="lead"><?php echo Yii::$app->user->identity->name ?></p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <?php
                if (Yii::$app->user->getId() == 1) {
                    echo
                    '<div class="col-lg-4">
                        <h2>Kelola Barang</h2>
        
                        <p>Tempat Mengelola Persediaan Barang di Sekolah</p>
        
                        <p><a class="btn btn-default" href="?r=inventory">&raquo;</a></p>
                    </div>';
                }                
            
            ?>
            <div class="col-lg-4">
                <h2>Peminjaman</h2>

                <p>Tempat Peminjaman Barang</p>

                <p><a class="btn btn-default" href="?BorrowsSearch%5Bid%5D=&BorrowsSearch%5Btanggal_pinjam%5D=&BorrowsSearch%5Btanggal_kembali%5D=&BorrowsSearch%5Bstatus_peminjaman%5D=pinjam&r=borrow">&raquo;</a></p>
            </div>
            <?php
                if (Yii::$app->user->getId() == 1 || Yii::$app->user->getId() == 2) {
                    echo
                    '
                    <div class="col-lg-4">
                    <h2>Pengembalian</h2>
    
                    <p>Tempat Pengembalian Barang</p>
    
                    <p><a class="btn btn-default" href="?r=return"> &raquo;</a></p>
                </div>';
            }
            ?>
        </div>
    </div>
</div>