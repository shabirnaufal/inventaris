<?php

namespace app\controllers;

use Yii;
use app\models\Borrows;
use app\models\BorrowsSearch;
use app\models\BorrowDetails;
use app\models\BorrowDetailsSearch;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Model;

/**
 * BorrowController implements the CRUD actions for Borrows model.
 */
class BorrowController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Borrows models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BorrowsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Borrows model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'details' => $this->findDetails($id),
        ]);
    }
     
    /**
     * Finds the Transactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Borrows::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     
    /**
     * Finds the TransactionDetails model based on its foreign key value.
     * @param integer $id
     * @return data provider TransactionDetails for GridView 
     */
    protected function findDetails($id)
    {
        $detailModel = new BorrowDetailsSearch();
        return $detailModel->search(['BorrowDetailsSearch'=>['id'=>$id]]);
    }

    /**
     * Creates a new Borrows model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Borrows();
        $details = [ new BorrowDetails ];
    
        // proses isi post variable 
        if ($model->load(Yii::$app->request->post())) {
            $details = Model::createMultiple(BorrowDetails::classname());
            Model::loadMultiple($details, Yii::$app->request->post());
    
            // assign default Borrow_id
            foreach ($details as $detail) {
                $detail->id = 0;
            }
    
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($details),
                    ActiveForm::validate($model)
                );
            }
    
            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;
    
            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database Borrow
                $Borrow = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // kemudian simpan detail records
                        foreach ($details as $detail) {
                            $detail->id = $model->id;
                            if (! ($flag = $detail->save(false))) {
                                $Borrow->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database Borrow
                        // kemudian tampilkan hasilnya
                        $Borrow->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        return $this->render('create', [
                            'model' => $model,
                            'details' => $details,
                        ]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database Borrow
                    $Borrow->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'details' => $details,
                    'error' => 'valid1: '.print_r($valid1,true).' - valid2: '.print_r($valid2,true),
                ]);
            }
    
        } else {
            // inisialisai id 
            // diperlukan untuk form master-detail
            $model->id = 0;
            // render view
            return $this->render('create', [
                'model' => $model,
                'details' => $details,
            ]);
        }
    }

    /**
     * Updates an existing Borrows model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $details = $model->polisi;
     
        if ($model->load(Yii::$app->request->post())) {
     
            $oldIDs = ArrayHelper::map($details, 'id', 'id');
            $details = Model::createMultiple(BorrowDetails::classname(), $details);
            Model::loadMultiple($details, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($details, 'id', 'id')));
     
            // assign default Borrow_id
            foreach ($details as $detail) {
                $detail->id = $model->id;
            }
     
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($details),
                    ActiveForm::validate($model)
                );
            }
     
            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;
     
            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database Borrow
                $Borrow = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // delete dahulu semua record yang ada
                        if (! empty($deletedIDs)) {
                            BorrowDetails::deleteAll(['id' => $deletedIDs]);
                        }
                        // kemudian, simpan details record
                        foreach ($details as $detail) {
                            $detail->id = $model->id;
                            if (! ($flag = $detail->save(false))) {
                                $Borrow->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database Borrow
                        // kemudian tampilkan hasilnya
                        $Borrow->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database Borrow
                    $Borrow->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'details' => $details,
                    'error' => 'valid1: '.print_r($valid1,true).' - valid2: '.print_r($valid2,true),
                ]);
            }
        }
     
        // render view
        return $this->render('update', [
            'model' => $model,
            'details' => (empty($details)) ? [new BorrowDetails] : $details
        ]);
    }

    /**
     * Deletes an existing Borrows model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $details = $model->polisi;
     
        // mulai database transaction
        $borrows = \Yii::$app->db->beginTransaction();
        try {
            // pertama, delete semua detail records
            foreach ($details as $detail) {
                $detail->delete();
            }
            // kemudian, delete master record
            $model->delete();
            // sukses, commit transaction
            $borrows->commit();
     
        } catch (Exception $e) {
            // gagal, rollback database transaction
            $borrows->rollBack();
        }
     
        return $this->redirect(['index']);
    }

    /**
     * Finds the Borrows model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Borrows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
}
